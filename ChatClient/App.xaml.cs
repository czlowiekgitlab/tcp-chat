﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ChatClient.Services;
using ChatClient.ViewModels;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.DependencyInjection;

namespace ChatClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Ioc.Default.ConfigureServices(
                new ServiceCollection()
                .AddSingleton<IServerService, ServerService>()
                .AddSingleton<MainWindowViewModel>()
                .BuildServiceProvider());
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            Ioc.Default.GetService<IServerService>().LogOut();
        }
    }
}
