﻿using ChatClient.Models;
using SharedClassLibrary;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChatClient.Messages
{
    public class ReceivedMessage
    {
        public ChatMessage Message { get; set; }
        public int ChatroomId { get; set; }
        public ReceivedMessage(ChatMessage chatMessage)
        {
            Message = chatMessage;
            ChatroomId = -1;
        }
        public ReceivedMessage(ChatMessage chatMessage, int chatroomId)
        {
            Message = chatMessage;
            ChatroomId = chatroomId;
        }
    }
}
