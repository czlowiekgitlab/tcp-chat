﻿using SharedClassLibrary;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChatClient.Models
{
    public class ChatMessage
    {
        public string Message { get; }
        public User Sender { get; }

        public ChatMessage(string message, User sender)
        {
            Message = message;
            Sender = sender;
        }
    }
}
