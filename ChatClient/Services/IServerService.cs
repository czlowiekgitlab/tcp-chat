﻿using SharedClassLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ChatClient.Services
{
    interface IServerService
    {
        public NetworkMessage LogInToServer(string username, string password);
        public NetworkMessage RegisterToServer(string username, string password);
        public void LogOut();
        public User LoggedInUser { get; set; }
        public ObservableCollection<User> ActiveUsers { get; set; }
        public ObservableCollection<PrivateChat> PrivateChats { get; set; }
        public void SendMessage(NetworkMessage message);
        public void CreateNewChat(List<int> checkedIdList);
    }
}
