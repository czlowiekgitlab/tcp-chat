﻿using ChatClient.Messages;
using ChatClient.Models;
using SharedClassLibrary;
using Microsoft.Toolkit.Mvvm.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ChatClient.Services
{
    class ServerService : IServerService
    {
        private const int TIMEOUT_DURATION = 10000;

        private TcpClient tcpClient;

        public User LoggedInUser { get; set; }
        public ObservableCollection<User> ActiveUsers { get; set; }
        public ObservableCollection<PrivateChat> PrivateChats { get; set; } = new ObservableCollection<PrivateChat>();

        public ServerService()
        {
            WeakReferenceMessenger.Default.Register<ServerService, UIMessage>(this, (r, m) => OnLogOut(m));
        }

        private void OnLogOut(UIMessage message)
        {
            if (message.EventType == UIMessage.UIEvent.LoggedOut)
                LogOut();
        }

        /// <summary>
        /// Loguje użytkownika do serwera za pomocą gniazdka TCP oraz wiadomości w formacie JSON
        /// Zwraca potwierdzenie/błędy logowania wysyłane przez serwer
        /// Zwraca null jeśli nie udało się połączyć z serwerem
        /// </summary>
        /// <param name="jsonRequest"></param>
        /// <returns></returns>
        public NetworkMessage LogInToServer(string username, string password)
        {
            if (!ConnectToServer())
                return null;

            JObject credentials = PackCredentials(username, password);

            NetworkMessage message = new NetworkMessage(ResponseCodes.LOGIN_REQUEST, "Login request", credentials);

            WriteMessageToStream(tcpClient.GetStream(), JsonConvert.SerializeObject(message));

            NetworkMessage response = JsonConvert.DeserializeObject<NetworkMessage>(ReadMessageFromStream(tcpClient.GetStream()));

            SwitchUser(response);

            return response;
        }

        /// <summary>
        /// Rejestruje użytkownika na serwerze za pomocą gniazdka TCP oraz wiadomości w formacie JSON
        /// Zwraca potwierdzenie/błędy rejestracji wysyłane przez serwer
        /// </summary>
        /// <param name="jsonRequest"></param>
        /// <returns></returns>
        public NetworkMessage RegisterToServer(string username, string password)
        {
            if (!ConnectToServer())
                return null;

            JObject credentials = PackCredentials(username, password);

            NetworkMessage message = new NetworkMessage(ResponseCodes.REGISTER_REQUEST, "Register request", credentials);

            WriteMessageToStream(tcpClient.GetStream(), JsonConvert.SerializeObject(message));

            NetworkMessage response = JsonConvert.DeserializeObject<NetworkMessage>(ReadMessageFromStream(tcpClient.GetStream()));

            SwitchUser(response);

            return response;
        }

        private static JObject PackCredentials(string username, string password)
        {
            JObject credentials = JObject.FromObject(new User(0, username));

            credentials.Add(new JProperty("Password", password));
            return credentials;
        }

        private void SwitchUser(NetworkMessage response)
        {
            if (response.ResponseCode == ResponseCodes.LOGIN_SUCCESSFUL
                            || response.ResponseCode == ResponseCodes.REGISTER_SUCCESSFUL)
            {
                ActiveUsers = response.Load[1].ToObject<ObservableCollection<User>>();
                LoggedInUser = response.Load[0].ToObject<User>();
                PrivateChats = new ObservableCollection<PrivateChat>(response.Load[2].ToObject<PrivateChat[]>());

                tcpClient.GetStream().ReadTimeout = TIMEOUT_DURATION;
                tcpClient.GetStream().WriteTimeout = TIMEOUT_DURATION;

                ListenToServerMessages();
            }
        }

        public void LogOut()
        {
            if (tcpClient == null)
            {
                return;
            }

            NetworkMessage message = new NetworkMessage(ResponseCodes.END_CONNECTION, "Koniec połączenia", LoggedInUser.Id);

            WeakReferenceMessenger.Default.Send(new UIMessage(UIMessage.UIEvent.Disconnected));

            try
            {
                WriteMessageToStream(tcpClient.GetStream(), JsonConvert.SerializeObject(message));
            }
            catch (System.ObjectDisposedException)
            {
                return;
            }

            tcpClient.Close();
        }

        private bool ConnectToServer()
        {
            tcpClient = new TcpClient();

            try
            {
                tcpClient.Connect(IPAddress.Loopback.ToString(), 9997);
            }
            catch
            {
                return false;
            }

            return true;
        }

        private void WriteMessageToStream(NetworkStream networkStream, string message)
        {
            // Dodaj znak Unicode end of text do końca wiadomości
            message += '\u0003';

            // Wyślij wiadomość
            byte[] messageBytes = Encoding.Unicode.GetBytes(message);
            try
            {
                networkStream.Write(messageBytes, 0, messageBytes.Length);
            }
            catch (System.IO.IOException)
            {
                LogOut();
            }
        }

        private string ReadMessageFromStream(NetworkStream networkStream)
        {
            string message = "";

            byte[] buffer = new byte[4096];

            // Czytaj strumień dopóki wiadmość nie skończy się znakiem Unicode end of text
            while (!message.EndsWith('\u0003'))
            {
                int byteCount;

                try
                {
                    byteCount = networkStream.Read(buffer, 0, buffer.Length);
                }
                catch (System.IO.IOException)
                {
                    LogOut();
                    return message;
                }

                message += Encoding.Unicode.GetString(buffer, 0, byteCount);
            }

            // Usuń znak Unicode end of text z końca wiadomości
            message = message.TrimEnd('\u0003');

            return message;
        }

        private async Task<string> ReadMessageFromStreamAsync(NetworkStream networkStream)
        {
            string message = "";

            byte[] buffer = new byte[4096];

            // Czytaj strumień dopóki wiadmość nie skończy się znakiem Unicode end of text
            while (!message.EndsWith('\u0003'))
            {
                int byteCount;

                try
                {
                    byteCount = await networkStream.ReadAsync(buffer, 0, buffer.Length);
                }
                catch (System.IO.IOException)
                {
                    LogOut();
                    return message;
                }

                message += Encoding.Unicode.GetString(buffer, 0, byteCount);
            }

            // Usuń znak Unicode end of text z końca wiadomości
            message = message.TrimEnd('\u0003');

            return message;
        }

        public void SendMessage(NetworkMessage message)
        {
            try
            {
                WriteMessageToStream(tcpClient.GetStream(), JsonConvert.SerializeObject(message));
            }
            catch (System.ObjectDisposedException)
            {
                LogOut();
                return;
            }
        }

        private async void ListenToServerMessages()
        {
            while (tcpClient.Connected)
            {
                string json;

                try
                {
                    json = await ReadMessageFromStreamAsync(tcpClient.GetStream());
                }
                catch
                {
                    tcpClient.Close();
                    LogOut();
                    return;
                }

                NetworkMessage message = JsonConvert.DeserializeObject<NetworkMessage>(json);

                if (message != null)
                    ProcessMessage(message);
            }
        }

        private void ProcessMessage(NetworkMessage message)
        {
            switch (message.ResponseCode)
            {              
                case ResponseCodes.UPADTE_USERLIST:
                    ActiveUsers = message.Load.ToObject<ObservableCollection<User>>();
                    WeakReferenceMessenger.Default.Send(new UIMessage(UIMessage.UIEvent.UserListUpdated));
                    break;

                case ResponseCodes.SEND_GLOBAL_MESSAGE:
                    ChatMessage chatMessage = new ChatMessage(message.Message, message.Load.First.ToObject<User>());
                    WeakReferenceMessenger.Default.Send(new ReceivedMessage(chatMessage));
                    break;

                case ResponseCodes.CHAT_CREATED:

                    PrivateChat createdChat = message.Load[0].ToObject<PrivateChat>();

                    List<User> users = message.Load[1].ToObject<List<User>>();

                    foreach (User user in users)
                    {
                        createdChat.AddUser(user);
                    }

                    PrivateChats.Insert(0, createdChat);

                    WeakReferenceMessenger.Default.Send(new UIMessage(UIMessage.UIEvent.ChatListUpdated));
                    break;

                case ResponseCodes.SEND_PRIVATE_CHAT_MESSAGE:
                    ChatMessage privateChatMessage = new ChatMessage(message.Message, message.Load.First.ToObject<User>());
                    WeakReferenceMessenger.Default.Send(new ReceivedMessage(privateChatMessage, message.Load[1].ToObject<int>()));
                    break;

                default:
                    return;
            }
        }

        public void CreateNewChat(List<int> idList)
        {
            NetworkMessage message = new NetworkMessage(ResponseCodes.CHAT_CREATE_REQUEST, "Stwórz nowy czat", idList);

            try
            {
                WriteMessageToStream(tcpClient.GetStream(), JsonConvert.SerializeObject(message));
            }
            catch (System.ObjectDisposedException)
            {
                LogOut();
                return;
            }
        }
    }
}
