﻿using ChatClient.Messages;
using ChatClient.Services;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.DependencyInjection;
using Microsoft.Toolkit.Mvvm.Input;
using Microsoft.Toolkit.Mvvm.Messaging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace ChatClient.ViewModels
{
    public class ChatViewModel : ObservableObject
    {
        private IServerService _serverService = Ioc.Default.GetService<IServerService>();

        private string _username;
        public string Username 
        {
            get => _username;
            set => SetProperty(ref _username, value);
        }

        private ObservableObject _currentTab;
        public ObservableObject CurrentTab
        {
            get => _currentTab;
            set => SetProperty(ref _currentTab, value);
        }

        private List<ObservableObject> _activeTabs = new List<ObservableObject>
        {
            new ChatGlobalViewModel(),
            new PrivateChatViewModel()
        };

        public ChatViewModel()
        {
            // Komendy
            LogOutCommand = new RelayCommand(LogOut);
            ChangeTabCommand = new RelayCommand<string>(ChangeTab);

            Username = _serverService.LoggedInUser.Username + $" ({_serverService.LoggedInUser.Id})";

            CurrentTab = _activeTabs[0];
        }

        // Komendy
        public ICommand LogOutCommand { get; }
        private void LogOut()
        {
            // Wyślij wiadomość o wylogowaniu
            WeakReferenceMessenger.Default.Send(new UIMessage(UIMessage.UIEvent.LoggedOut));
        }

        public ICommand ChangeTabCommand { get; }
        private void ChangeTab(string tabIndex)
        {
            // Zmień zakładkę
            CurrentTab = _activeTabs[int.Parse(tabIndex)];
        }
    }
}
