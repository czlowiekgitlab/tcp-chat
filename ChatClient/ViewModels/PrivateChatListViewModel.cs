﻿using ChatClient.Messages;
using ChatClient.Services;
using ChatClient.Views;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.DependencyInjection;
using Microsoft.Toolkit.Mvvm.Input;
using Microsoft.Toolkit.Mvvm.Messaging;
using SharedClassLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;

namespace ChatClient.ViewModels
{
    public class PrivateChatListViewModel : ObservableRecipient
    {
        private IServerService _serverService = Ioc.Default.GetService<IServerService>();

        private ObservableCollection<PrivateChat> _privateChats;
        public ObservableCollection<PrivateChat> PrivateChats
        {
            get => _privateChats;
            set => SetProperty(ref _privateChats, value);
        }

        public PrivateChatListViewModel()
        {
            CreateNewChatCommand = new RelayCommand(CreateNewChat);
            GoToChatCommand = new RelayCommand<int>(GoToChat);

            PrivateChats = _serverService.PrivateChats;

            Messenger.Register<PrivateChatListViewModel, UIMessage>(this, (r, m) => r.OnChatUpdate(m));
        }

        private void OnChatUpdate(UIMessage message)
        {
            if (message.EventType == UIMessage.UIEvent.ChatListUpdated)
                PrivateChats = _serverService.PrivateChats;
        }

        // Komendy
        public ICommand CreateNewChatCommand { get; }
        private void CreateNewChat()
        {
            // MVVM violation
            PrivateChatChooseUsersWindowViewModel viewModel = new PrivateChatChooseUsersWindowViewModel();
            PrivateChatChooseUsersWindowView window = new PrivateChatChooseUsersWindowView();

            window.DataContext = viewModel;

            bool test = (bool)window.ShowDialog();

            if (test && viewModel.CheckedIdList.Count != 0)
            {
                _serverService.CreateNewChat(viewModel.CheckedIdList);
            }
        }

        public ICommand GoToChatCommand { get; }
        private void GoToChat(int chatId)
        {
            WeakReferenceMessenger.Default.Send<UIMessage>(new UIMessage(UIMessage.UIEvent.PrivateChatClick, chatId.ToString()));
        }
    }
}
