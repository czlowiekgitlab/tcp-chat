﻿using ChatClient.Messages;
using ChatClient.Models;
using ChatClient.Services;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.DependencyInjection;
using Microsoft.Toolkit.Mvvm.Input;
using Microsoft.Toolkit.Mvvm.Messaging;
using SharedClassLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace ChatClient.ViewModels
{
    public class PrivateChatRoomViewModel : ObservableRecipient
    {
        private IServerService _serverService = Ioc.Default.GetService<IServerService>();

        private ObservableCollection<ChatMessage> _chatMessages = new ObservableCollection<ChatMessage>();
        public ObservableCollection<ChatMessage> ChatMessages
        {
            get => _chatMessages;
            set => SetProperty(ref _chatMessages, value);
        }

        private ObservableCollection<User> _userList;

        public ObservableCollection<User> UserList
        {
            get => _userList;
            set => SetProperty(ref _userList, value);
        }

        private string _messageToSend;
        public string MessageToSend
        {
            get => _messageToSend;
            set => SetProperty(ref _messageToSend, value);
        }
        public int ChatroomId { get; }

        private PrivateChat _chatroom;

        public PrivateChatRoomViewModel(int chatroomId)
        {
            ReturnToChatListCommand = new RelayCommand(ReturnToChatList);

            ChatroomId = chatroomId;

            _chatroom = _serverService.PrivateChats.Single(chat => chat.Id == chatroomId);

            var activeUsersInChatroom = from userInChatroom in _chatroom.Users
                                        join userActive in _serverService.ActiveUsers on userInChatroom.Id equals userActive.Id
                                        select userInChatroom;

            UserList = new ObservableCollection<User>(activeUsersInChatroom);

            SendMessageCommand = new RelayCommand(SendMessage);

            MessageToSend = "";

            Messenger.Register<PrivateChatRoomViewModel, UIMessage>(this, (r, m) => r.OnUserListUpdate(m));
            Messenger.Register<PrivateChatRoomViewModel, ReceivedMessage>(this, (r, m) => r.OnMessageReceived(m));
        }

        private void OnUserListUpdate(UIMessage message)
        {
            if (message.EventType == UIMessage.UIEvent.UserListUpdated && _chatroom != null)
            {
                var activeUsersInChatroom = from userInChatroom in _chatroom.Users
                                            join userActive in _serverService.ActiveUsers on userInChatroom.Id equals userActive.Id
                                            select userInChatroom;

                UserList = new ObservableCollection<User>(activeUsersInChatroom);
            }      
        }

        private void OnMessageReceived(ReceivedMessage receivedMessage)
        {
            if (receivedMessage.ChatroomId == ChatroomId)
                ChatMessages.Add(receivedMessage.Message);
        }

        // Komendy
        public ICommand SendMessageCommand { get; set; }
        private void SendMessage()
        {
            object[] load = new object[]
            {
                _serverService.LoggedInUser,
                ChatroomId
            };

            NetworkMessage networkMessage = new NetworkMessage(ResponseCodes.SEND_PRIVATE_CHAT_MESSAGE, MessageToSend, load);

            _serverService.SendMessage(networkMessage);

            MessageToSend = "";
        }

        public ICommand ReturnToChatListCommand { get; set; }
        private void ReturnToChatList()
        {
            Messenger.Send<UIMessage>(new UIMessage(UIMessage.UIEvent.ReturnToChatList));
        }
    }
}
