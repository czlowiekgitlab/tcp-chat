﻿using ChatClient.Messages;
using ChatClient.Services;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.DependencyInjection;
using Microsoft.Toolkit.Mvvm.Messaging;
using SharedClassLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ChatClient.ViewModels
{
    public class PrivateChatViewModel : ObservableRecipient
    {
        private ObservableObject _currentView;
        private IServerService _serverService = Ioc.Default.GetService<IServerService>();
        private PrivateChatListViewModel _chatList;

        public ObservableObject CurrentView
        {
            get => _currentView;
            set => SetProperty(ref _currentView, value);
        }

        ObservableCollection<PrivateChatRoomViewModel> _chatrooms = new ObservableCollection<PrivateChatRoomViewModel>();
        public ObservableCollection<PrivateChatRoomViewModel> Chatrooms
        {
            get => _chatrooms;
            set => SetProperty(ref _chatrooms, value);
        } 

        public PrivateChatViewModel()
        {
            _chatList = new PrivateChatListViewModel();

            CurrentView = _chatList;

            foreach (PrivateChat privateChat in _serverService.PrivateChats)
            {
                _chatrooms.Add(new PrivateChatRoomViewModel(privateChat.Id));
            }

            Messenger.Register<PrivateChatViewModel, UIMessage>(this, (r, m) => OnUIMessage(m));
        }
        private void OnUIMessage(UIMessage message)
        {
            if (message.EventType == UIMessage.UIEvent.ChatListUpdated)
            {
                Chatrooms.Add(new PrivateChatRoomViewModel(_serverService.PrivateChats[0].Id));
            }
            else if (message.EventType == UIMessage.UIEvent.PrivateChatClick)
            {
                CurrentView = Chatrooms.First(chatroom => chatroom.ChatroomId == int.Parse(message.Message));
            }
            else if (message.EventType == UIMessage.UIEvent.ReturnToChatList)
            {
                CurrentView = _chatList;
            }
        }
    }
}
