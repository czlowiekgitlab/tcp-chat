﻿using ChatServer.Models;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using Newtonsoft.Json.Linq;
using SharedClassLibrary;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace ChatServer
{
    class Program
    {
        private const int TIMEOUT_DURATION = 10000;

        private static List<UserStored> _userList = new List<UserStored>();
        private static ObservableCollection<ActiveUser> _activeUsers = new ObservableCollection<ActiveUser>();
        private static ObservableCollection<PrivateChat> _privateChats = new ObservableCollection<PrivateChat>();

        static async Task Main(string[] args)
        {
            _activeUsers.CollectionChanged += _activeUsers_CollectionChanged;

            await StartListening();
        }

        private static void _activeUsers_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            User[] users = GetActiveUsers();

            JObject json = JObject.FromObject(new NetworkMessage(ResponseCodes.UPADTE_USERLIST,
                "Lista aktywnych użytkowników",
                users));

            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                foreach (ActiveUser activeUser in _activeUsers)
                {
                    if (e.NewItems.Contains(activeUser))
                        continue;

                    _ = WriteMessageToStream(activeUser.TcpClient.GetStream(), json.ToString());
                }
            }
            else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                foreach (ActiveUser activeUser in _activeUsers)
                {
                    _ = WriteMessageToStream(activeUser.TcpClient.GetStream(), json.ToString());
                }
            }
        }

        // Zaimplementowane bo TcpClient buguje serializer Json
        private static User[] GetActiveUsers()
        {
            User[] users = new User[_activeUsers.Count];

            for (int i = 0; i < users.Length; i++)
            {
                users[i] = new User(_activeUsers[i].Id, _activeUsers[i].Username);
            }

            return users;
        }

        public static async Task StartListening()
        {
            // Port oraz lokalny adres IP
            int port = 9997;
            IPAddress localAddr = IPAddress.Any;

            // Zacznij nasłuchiwać połączeń przychodzących
            TcpListener listener = new TcpListener(localAddr, port);

            listener.Start();

            Console.WriteLine("[Serwer] Oczekiwanie na połączenie...");

            // Nasłuchuj połączeń w nieskończoność
            while (true)
            {
                try
                {
                    // Akceptacja połączenia
                    TcpClient tcpClient = await listener.AcceptTcpClientAsync();

                    // Przetwórz przychodzące połącznie
                    AcceptConnection(tcpClient);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        // Przetwarza połączenie przychodzące
        public static async void AcceptConnection(TcpClient tcpClient)
        {
            Console.WriteLine($"[Serwer] Połączono z klientem: " +
                $"{((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address}");

            NetworkStream networkStream = tcpClient.GetStream();
            ActiveUser user = null;
            bool keepConnection = false;

            // Ustaw timeout dla czytania i pisania do strumienia sieciowego
            networkStream.WriteTimeout = TIMEOUT_DURATION;
            networkStream.ReadTimeout = TIMEOUT_DURATION;

            // Przechwytuj błędy zwiazane z strumieniem sieci
            try
            {
                // Odbierz pierwszą wiadomość od klienta (Logowanie/Rejestracja)
                // Przekonwertuj json z wiadomości
                NetworkMessage request = JObject
                    .Parse(await ReadMessageFromStream(networkStream))
                    .ToObject<NetworkMessage>();

                // Inicjalizacja zmiennych
                string responseMessage = "";
                int responseCode;

                // Rozpatrz przypadki wiadomości
                switch (request.ResponseCode)
                {
                    case ResponseCodes.REGISTER_REQUEST:
                        user = UserRegister(tcpClient, request);

                        if (user != null)
                        {
                            keepConnection = true;
                            responseCode = 11;
                            responseMessage = "Zarejestrowano użytkownika";
                        }
                        else
                        {
                            responseCode = 12;
                            responseMessage = "Nazwa użytkownika zajęta";
                        }

                        break;

                    case ResponseCodes.LOGIN_REQUEST:
                        user = UserLogin(tcpClient, request);

                        if (user != null)
                        {
                            keepConnection = true;
                            responseCode = 21;
                            responseMessage = "Zalogowano użytkownika";
                        }
                        else
                        {
                            responseCode = 22;
                            responseMessage = "Nieprawidłowa nazwa użytkownika lub hasło";
                        }

                        break;

                    default:
                        responseCode = 0;
                        responseMessage = "Wystąpił błąd";
                        break;
                }

                NetworkMessage response;

                if (keepConnection)
                    // new User, bo się serializer buguje
                    response = new NetworkMessage(responseCode, responseMessage, new object[] { new User(user.Id, user.Username),
                        GetActiveUsers(), _privateChats.Where(chat => chat.Users.Any(userChecked => userChecked.Id == user.Id)).ToArray() });
                else
                    response = new NetworkMessage(responseCode, responseMessage);

                // Wyślij klientowi odpowiedź serwera
                await WriteMessageToStream(networkStream, JsonConvert.SerializeObject(response));
            }
            catch (Exception ex)
            {
                Console.WriteLine("[Serwer] " + ex.Message);

                DisconnectAnonumousClient(tcpClient);

                return;
            }

            // Jeśli klient został zarejestrowany lub zalogowany...
            if (keepConnection)
                try
                {
                    // Oczekuj wiadomości od tego klienta
                    await ListenForClientMessages(user);
                }
                catch (System.ObjectDisposedException ex)
                {
                    
                }
            else
            {
                DisconnectAnonumousClient(tcpClient);
            }
        }

        private static void DisconnectAnonumousClient(TcpClient tcpClient)
        {
            Console.WriteLine($"[Serwer:anonymous@{((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address}] " +
                $"Rozłączono");

            // Zamknij strumień i połączenie
            tcpClient.GetStream().Close();
            tcpClient.Close();
        }

        private static ActiveUser UserLogin(TcpClient tcpClient, NetworkMessage request)
        {
            
            // Znajdź zarejestrowanego użytkownika z odpowiadającymi danymi (zwraca null, jeśli nie odnaleziono)
            UserStored user = _userList.Where(user =>
                String.Equals(user.Username, request.Load.First.Value<string>("Username"), StringComparison.OrdinalIgnoreCase)
                &&
                String.Equals(user.Password, request.Load.First.Value<string>("Password")))
                    .SingleOrDefault();

            if (user != null)
            {
                Console.WriteLine($"[Serwer:{user.Username}@{((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address}] " +
                    $"Zalogowano");
            }
            else
            {
                Console.WriteLine($"[Serwer:anonymous@{((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address}] " +
                    $"Nieprawidłowa nazwa użytkownika lub hasło");

                return null;
            }

            ActiveUser activeUser = new ActiveUser(user.Id, user.Username, tcpClient);

            _activeUsers.Add(activeUser);

            return activeUser;
        }

        private static ActiveUser UserRegister(TcpClient tcpClient, NetworkMessage request)
        {

            // Sprawdź czy istnieje użytkownik z taką samą nazwą
            foreach (UserStored registeredUser in _userList)
            {
                if (String.Equals(registeredUser.Username, request.Load.First.Value<string>("Username"),
                    StringComparison.OrdinalIgnoreCase))
                {
                    Console.WriteLine($"[Serwer:anonymous@{((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address}] " +
                        $"Nie zarejestrowano - nazwa użytkownika zajęta");

                    return null;
                }
            }

            // Jeśli nie istnieje taki użytkownik to go stwórz
            UserStored user = new UserStored(request.Load.First.Value<string>("Username"),
                request.Load.First.Value<string>("Password"),
                tcpClient);

            // Dodaj do listy aktywnych użytkowników
            _userList.Add(user);

            Console.WriteLine($"[Serwer:{user.Username}@{((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address}] " +
                $"Zarejestrowano użytkownika {user.Username}");

            ActiveUser activeUser = new ActiveUser(user.Id, user.Username, tcpClient);

            _activeUsers.Add(activeUser);

            return activeUser;
        }

        private static async Task WriteMessageToStream(NetworkStream networkStream, string message)
        {
            // Dodaj znak Unicode end of text do końca wiadomości
            message += '\u0003';

            // Wyślij wiadomość
            byte[] messageBytes = Encoding.Unicode.GetBytes(message);
            await networkStream.WriteAsync(messageBytes, 0, messageBytes.Length);
        }

        private static async Task<string> ReadMessageFromStream(NetworkStream networkStream)
        {
            string message = "";

            byte[] buffer = new byte[4096];

            // Czytaj strumień dopóki wiadmość nie skończy się znakiem Unicode end of text
            while (!message.EndsWith('\u0003'))
            {
                int byteCount = await networkStream.ReadAsync(buffer, 0, buffer.Length);
                message += Encoding.Unicode.GetString(buffer, 0, byteCount);
            }

            // Usuń znak Unicode end of text z końca wiadomości
            message = message.TrimEnd('\u0003');
            
            return message;
        }

        public static async Task ListenForClientMessages(ActiveUser user)
        {
            try
            {
                while (user.TcpClient.Connected)
                {
                    NetworkStream networkStream = user.TcpClient.GetStream();

                    // Odczytuj wiadomości danego klienta
                    string message = await ReadMessageFromStream(networkStream);

                    NetworkMessage networkMessage = JsonConvert.DeserializeObject<NetworkMessage>(message);

                    await ProcessMessage (networkMessage);
                }
            }
            catch
            {
                DisconnectActiveUser(user);
                return;
            }
        }

        private async static Task ProcessMessage(NetworkMessage networkMessage)
        {
            switch (networkMessage.ResponseCode)
            {
                case ResponseCodes.SEND_GLOBAL_MESSAGE:
                    await SendGlobalMessage(networkMessage);
                    break;

                case ResponseCodes.CHAT_CREATE_REQUEST:
                    List<int> idList = networkMessage.Load.First.ToObject<List<int>>();
                    CreatePrivateChat(idList);
                    break;

                case ResponseCodes.SEND_PRIVATE_CHAT_MESSAGE:
                    await SendPrivateChatMessage(networkMessage);
                    break;

                case ResponseCodes.END_CONNECTION:
                    DisconnectActiveUser(_activeUsers.First(user => user.Id == networkMessage.Load.First.ToObject<int>()));
                    break;

            }
        }

        private static void CreatePrivateChat(List<int> idList)
        {
            PrivateChat privateChat = new PrivateChat();

            foreach (User user in _userList)
            {
                if (idList.Contains(user.Id))
                    privateChat.AddUser(new User(user.Id, user.Username));
            }

            privateChat.UsersChanged += PrivateChat_UsersChanged;

            _privateChats.Add(privateChat);    

            foreach (User user in privateChat.Users)
            {
                ActiveUser recipient = _activeUsers.First(activeUser => activeUser.Id == user.Id);

                object[] load = new object[]
                {
                    privateChat,
                    privateChat.Users
                };

                NetworkMessage networkMessage = new NetworkMessage(ResponseCodes.CHAT_CREATED, "Stworzono nowy czat", load);

                _ = WriteMessageToStream(recipient.TcpClient.GetStream(), JsonConvert.SerializeObject(networkMessage));
            }

            Console.WriteLine($"[Serwer] Stworzono nowy czat prywatny {privateChat.Id}");
        }

        private async static Task SendGlobalMessage(NetworkMessage message)
        {
            foreach(ActiveUser activeUser in _activeUsers)
            {
                await WriteMessageToStream(activeUser.TcpClient.GetStream(), JsonConvert.SerializeObject(message));
            }

            Console.WriteLine($"{message.Load.First.Value<string>("Username")}: {message.Message}");
        }

        private async static Task SendPrivateChatMessage(NetworkMessage networkMessage)
        {
            int chatroomId = networkMessage.Load[1].ToObject<int>();

            PrivateChat destinationChat = _privateChats.First(chat => chat.Id == chatroomId);

            int[] userIds = new int[destinationChat.Users.Count()];

            for (int i = 0; i < destinationChat.Users.Count(); i++)
                userIds[i] = destinationChat.Users[i].Id;

            ActiveUser[] recipients = _activeUsers.Where(user => userIds.Contains(user.Id)).ToArray();

            foreach (ActiveUser recipient in recipients)
            {
                await WriteMessageToStream(recipient.TcpClient.GetStream(), JsonConvert.SerializeObject(networkMessage));
            }
        }

        private static void DisconnectActiveUser(ActiveUser user)
        {
            if (!user.TcpClient.Connected)
                return;

            Console.WriteLine($"[Serwer:{user.Username}@{((IPEndPoint)user.TcpClient.Client.RemoteEndPoint).Address}] " +
                $"Uzytkownik rozłączony");

            try
            {
                user.TcpClient.Close();
            }
            catch (System.ObjectDisposedException ex)
            {
                
            }

            _activeUsers.Remove(user);
        }

        private static async void PrivateChat_UsersChanged(object sender, PrivateChat.UserChangedEventArgs e)
        {
            PrivateChat privateChat = (PrivateChat)sender;

            List<User> users = privateChat.GetUsers();

            int[] userIds = new int[users.Count];

            for (int i = 0; i < users.Count; i++)
                userIds[i] = users[i].Id;

            if (e.newUser != null)
            {
                object[] load = new object[]
                {
                    new User(e.newUser.Id, e.newUser.Username),
                    privateChat.Id
                };

                NetworkMessage networkMessage = new NetworkMessage(ResponseCodes.CHAT_USER_ADD,
                    $"Dodano użytkownika {e.newUser.Username}({e.newUser.Id}) do czatu {privateChat.Id}", load);

                ActiveUser[] recipents = _activeUsers.Where(user => userIds.Contains(user.Id)).ToArray();

                foreach (ActiveUser recipent in recipents)
                {
                    await WriteMessageToStream(recipent.TcpClient.GetStream(),
                    JsonConvert.SerializeObject(networkMessage));
                }
            }

            if (e.oldUser != null)
            {
                object[] load = new object[]
                {
                    new User(e.oldUser.Id, e.oldUser.Username),
                    new JProperty("ChatId", privateChat.Id)
                };

                NetworkMessage networkMessage = new NetworkMessage(ResponseCodes.CHAT_USER_REMOVE,
                    $"Usunięto użytkownika {e.newUser.Username}({e.newUser.Id}) z czatu {privateChat.Id}", load);

                ActiveUser[] recipents = _activeUsers.Where(user => userIds.Contains(user.Id)).ToArray();

                foreach (ActiveUser recipent in recipents)
                {
                    await WriteMessageToStream(recipent.TcpClient.GetStream(),
                    JsonConvert.SerializeObject(networkMessage));
                    Thread.Sleep(500);
                }
            }
        }
    }
}
