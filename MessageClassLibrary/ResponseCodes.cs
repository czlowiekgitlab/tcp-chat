﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedClassLibrary
{
    public static class ResponseCodes
    {
        // Rejestracja
        public const int REGISTER_REQUEST = 10;
        public const int REGISTER_SUCCESSFUL = 11;
        public const int REGISTER_FAILED = 12;

        // Logowanie
        public const int LOGIN_REQUEST = 20;
        public const int LOGIN_SUCCESSFUL = 21;
        public const int LOGIN_FAILED  = 22;

        // Aktywni użytkownicy
        public const int UPADTE_USERLIST = 30;

        // Zakończenie połączenia
        public const int END_CONNECTION = 40;

        // Wysyłanie wiadomości globalnej
        public const int SEND_GLOBAL_MESSAGE = 50;

        // Czat prywatny
        public const int CHAT_CREATE_REQUEST = 60;
        public const int CHAT_CREATED = 61;
        public const int CHAT_FAILED = 62;
        public const int CHAT_USER_ADD = 63;
        public const int CHAT_USER_REMOVE = 64;

        // Wysyłanie wiadomości do prywatnego czatu
        public const int SEND_PRIVATE_CHAT_MESSAGE = 70;
    }
}
